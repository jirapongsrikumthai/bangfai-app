﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace SendMQTT
{
    public partial class SendMQTT : Form
    {
        static SerialPort _serialPort;
        string dataIN = "";
        MqttClient client;
        private bool _conMQTT = false;
        List<int> data = new List<int>();

        public SendMQTT()
        {
            InitializeComponent();
        }

        private void SendMQTT_Load(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            cboPort.Items.AddRange(ports);
            if(cboPort.Items.Count > 0)
                cboPort.SelectedIndex = 0;
            btnCloseCP.Enabled = false;

            _serialPort = new SerialPort();
            _serialPort.DataReceived += new SerialDataReceivedEventHandler(serialPort_DataReceived);
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                _serialPort.PortName = cboPort.Text;
                _serialPort.Open();
                btnOpenCP.Enabled = false;
                btnCloseCP.Enabled = true;
                gbMQTT.Enabled = true;
                lbStatusCP.Text = "( " + _serialPort.PortName + " ) Opened ";
                lbStatusCP.ForeColor = Color.Green;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Message",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                _serialPort.Close();
                btnOpenCP.Enabled = true;
                btnCloseCP.Enabled = false;

                hostDisconnect();

                gbMQTT.Enabled = false;

                lbStatusCP.Text = "( " + _serialPort.PortName + " ) Closed";
                lbStatusCP.ForeColor = Color.Red;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                SerialPort sp = (SerialPort)sender;
                int bytes = _serialPort.BytesToRead;
                byte[] buffer = new byte[bytes];

                _serialPort.Read(buffer,0, bytes);
                data.Clear();
                if (bytes == 32)
                {
                    foreach (byte number in buffer)
                    {
                        if (number == 254)
                            data.Add(10);//data[index++] = 10;
                        else
                            data.Add(number);//data[index++] = number;
                    }
                    dataIN = "#" + data[5].ToString() +
                                     "," + data[6].ToString() + "." + data[7].ToString() + data[8].ToString() + data[9].ToString() +      // latitude
                                     "," + data[10].ToString() + "." + data[11].ToString() + data[12].ToString() + data[13].ToString() +   // longitudes
                                     "," + Convert.ToInt32(data[14].ToString() + data[15].ToString()).ToString() +                        // alt
                                     "," + data[16].ToString() +                                                                          // temp
                                     "," + Convert.ToInt32(data[17].ToString() + (data[18] < 10? "0" + data[18].ToString() : data[18].ToString())).ToString() +//Convert.ToInt32(data[17].ToString() + data[18].ToString()).ToString() +                        // pressure
                                     "," + data[19].ToString() +                                                                          // volte
                                     "," + data[20].ToString() + ":" + data[21].ToString() + ":" + data[22].ToString() +                  // HH:mm:ss
                                     "," + (Convert.ToInt32(data[29].ToString()) / 100 == 2 ? "-" : "") + Convert.ToInt32(data[23].ToString() + data[24].ToString()).ToString() +                        // Gyro x
                                     "," + (Convert.ToInt32(data[29].ToString().Substring(1, data[29].ToString().Length - 1)) / 10 == 2 ? "-" : "") + Convert.ToInt32(data[25].ToString() + data[26].ToString()).ToString() +                        // Gyro y
                                     "," + (Convert.ToInt32(data[29].ToString().Substring(2, data[29].ToString().Length - 2)) / 1 == 2 ? "-" : "") + Convert.ToInt32(data[27].ToString() + data[28].ToString()).ToString() +                        // Gyro z
                                     "\n";
                    if (dataIN.Length > 0)
                        this.Invoke(new EventHandler(ShowData));
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            
        }

        private void ShowData(object sender, EventArgs e)
        {
            ledRecived.BackColor = Color.Lime;
            tmRecive.Start();
            if (_conMQTT)
            {
                Console.Write(dataIN);
                string topic = txtBoxTopic.Text.ToString().Trim();
                client.Publish(topic, Encoding.ASCII.GetBytes(dataIN));
                ledSendMQTT.BackColor = Color.Lime;
                tmMQTT.Start();

            }
        }

        private void tmRecive_Tick(object sender, EventArgs e)
        {
            tmRecive.Stop();
            ledRecived.BackColor = Color.Gray;
        }

        private void btnOpenHost_Click(object sender, EventArgs e)
        {
            try
            {
                string host = txtBoxHost.Text.ToString().Trim();
                client = new MqttClient(IPAddress.Parse(host));
                byte code = client.Connect(Guid.NewGuid().ToString());
                _conMQTT = true;
                btnOpenHost.Enabled = false;
                btnCloseHost.Enabled = true;
                lbStatusHost.Text = "MQTT Opened";
                lbStatusHost.ForeColor = Color.Green;
            }
            catch{
                _conMQTT = false;
            }
            
        }

        private void btnCloseHost_Click(object sender, EventArgs e)
        {
            hostDisconnect();
        }

        private void hostDisconnect()
        {
            try
            {
                if(_conMQTT)
                    client.Disconnect();
                btnOpenHost.Enabled = true;
                btnCloseHost.Enabled = false;
                lbStatusHost.Text = "MQTT Closed";
                lbStatusHost.ForeColor = Color.Red;
                _conMQTT = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void tmMQTT_Tick(object sender, EventArgs e)
        {
            tmMQTT.Stop();
            ledSendMQTT.BackColor = Color.Gray;
        }
        private void client_MqttMsgPublished(object sender, MqttMsgPublishedEventArgs e)
        {
            Console.WriteLine("Sucesss");
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            try
            {
                if (_conMQTT)
                {
                    string topic = txtBoxTopic.Text.ToString().Trim();
                    if (cbTest.Checked)
                        client.Publish(topic, Encoding.ASCII.GetBytes(txtBoxPayload.Text.ToString().Trim()));
                    else
                        client.Publish(topic, Encoding.ASCII.GetBytes("#sensor_id,13.189267,100.975472,10,10,51,151515,12,12,12"));
                    ledSendMQTT.BackColor = Color.Lime;
                    tmMQTT.Start();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbTest_CheckStateChanged(object sender, EventArgs e)
        {

            if (cbTest.Checked)
                gbTest.Enabled = true;
            else
                gbTest.Enabled = false;
        }

        private void btnCloseHost_EnabledChanged(object sender, EventArgs e)
        {
            if (btnCloseHost.Enabled && gbMQTT.Enabled)
                cbTest.Enabled = true;
            else
            {
                cbTest.Enabled = false;
                cbTest.Checked = false;
            } 
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            byte[] buffer = { 35, 255, 255, 0, 0, 13, 20, 80, 30, 100, 93, 79, 30, 0, 254, 31, 254, 14, 75, 9, 44, 42, 16, 0, 65, 52, 65, 44, 0, 0, 0, 0};
            int index = 0;
            if(buffer.Count() == 32)
            {
                foreach (byte number in buffer)
                {
                    if (number == 254)
                        data[index++] = 10;
                    else
                        data[index++] = number;
                }
                string dataIN = "#" + data[5].ToString() + "." + data[6].ToString() + data[7].ToString() + data[8].ToString() +      // latitude
                                "," + data[9].ToString() + "." + data[10].ToString() + data[11].ToString() + data[12].ToString() +   // longitudes
                                "," + Convert.ToInt32(data[13].ToString() + data[14].ToString()).ToString() +                        // alt
                                "," + data[15].ToString() +                                                                          // temp
                                "," + Convert.ToInt32(data[16].ToString() + data[17].ToString()).ToString() +                        // pressure
                                "," + data[18].ToString() +                                                                          // volte
                                "," + data[19].ToString() + ":" + data[20].ToString() + ":" + data[21].ToString() +                  // HH:mm:ss
                                "," + Convert.ToInt32(data[22].ToString() + data[23].ToString()).ToString() +                        // Gyro x
                                "," + Convert.ToInt32(data[24].ToString() + data[25].ToString()).ToString() +                        // Gyro y
                                "," + Convert.ToInt32(data[26].ToString() + data[27].ToString()).ToString() +                        // Gyro z
                                "\n";
            }
        }

        private void btnRefrash_Click(object sender, EventArgs e)
        {
            try
            {
                cboPort.Items.Clear();
                string[] ports = SerialPort.GetPortNames();
                cboPort.Items.AddRange(ports);
                if (cboPort.Items.Count > 0)
                    cboPort.SelectedIndex = 0;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
