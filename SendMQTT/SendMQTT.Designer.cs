﻿namespace SendMQTT
{
    partial class SendMQTT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SendMQTT));
            this.cboPort = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOpenCP = new System.Windows.Forms.Button();
            this.btnCloseCP = new System.Windows.Forms.Button();
            this.serialPort = new System.IO.Ports.SerialPort(this.components);
            this.label5 = new System.Windows.Forms.Label();
            this.lbStatusCP = new System.Windows.Forms.Label();
            this.ledRecived = new System.Windows.Forms.Panel();
            this.tmRecive = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRefrash = new System.Windows.Forms.Button();
            this.gbMQTT = new System.Windows.Forms.GroupBox();
            this.txtBoxTopic = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBoxHost = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnOpenHost = new System.Windows.Forms.Button();
            this.btnCloseHost = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.ledSendMQTT = new System.Windows.Forms.Panel();
            this.lbStatusHost = new System.Windows.Forms.Label();
            this.tmMQTT = new System.Windows.Forms.Timer(this.components);
            this.btnTest = new System.Windows.Forms.Button();
            this.gbTest = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtBoxPayload = new System.Windows.Forms.TextBox();
            this.cbTest = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.gbMQTT.SuspendLayout();
            this.gbTest.SuspendLayout();
            this.SuspendLayout();
            // 
            // cboPort
            // 
            this.cboPort.FormattingEnabled = true;
            this.cboPort.Location = new System.Drawing.Point(43, 16);
            this.cboPort.Name = "cboPort";
            this.cboPort.Size = new System.Drawing.Size(130, 21);
            this.cboPort.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Port:";
            // 
            // btnOpenCP
            // 
            this.btnOpenCP.Location = new System.Drawing.Point(179, 14);
            this.btnOpenCP.Name = "btnOpenCP";
            this.btnOpenCP.Size = new System.Drawing.Size(55, 23);
            this.btnOpenCP.TabIndex = 2;
            this.btnOpenCP.Text = "Open";
            this.btnOpenCP.UseVisualStyleBackColor = true;
            this.btnOpenCP.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnCloseCP
            // 
            this.btnCloseCP.Enabled = false;
            this.btnCloseCP.Location = new System.Drawing.Point(240, 14);
            this.btnCloseCP.Name = "btnCloseCP";
            this.btnCloseCP.Size = new System.Drawing.Size(50, 23);
            this.btnCloseCP.TabIndex = 3;
            this.btnCloseCP.Text = "Close";
            this.btnCloseCP.UseVisualStyleBackColor = true;
            this.btnCloseCP.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // serialPort
            // 
            this.serialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort_DataReceived);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Status:";
            // 
            // lbStatusCP
            // 
            this.lbStatusCP.AutoSize = true;
            this.lbStatusCP.Location = new System.Drawing.Point(55, 48);
            this.lbStatusCP.Name = "lbStatusCP";
            this.lbStatusCP.Size = new System.Drawing.Size(10, 13);
            this.lbStatusCP.TabIndex = 5;
            this.lbStatusCP.Text = "-";
            // 
            // ledRecived
            // 
            this.ledRecived.BackColor = System.Drawing.Color.Gray;
            this.ledRecived.Location = new System.Drawing.Point(150, 67);
            this.ledRecived.Name = "ledRecived";
            this.ledRecived.Size = new System.Drawing.Size(113, 25);
            this.ledRecived.TabIndex = 7;
            // 
            // tmRecive
            // 
            this.tmRecive.Interval = 200;
            this.tmRecive.Tick += new System.EventHandler(this.tmRecive_Tick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 18);
            this.label3.TabIndex = 8;
            this.label3.Text = "Recive Data :";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.btnRefrash);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cboPort);
            this.groupBox1.Controls.Add(this.btnOpenCP);
            this.groupBox1.Controls.Add(this.btnCloseCP);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.ledRecived);
            this.groupBox1.Controls.Add(this.lbStatusCP);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(300, 100);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Com Port";
            // 
            // btnRefrash
            // 
            this.btnRefrash.Location = new System.Drawing.Point(201, 38);
            this.btnRefrash.Name = "btnRefrash";
            this.btnRefrash.Size = new System.Drawing.Size(72, 23);
            this.btnRefrash.TabIndex = 16;
            this.btnRefrash.Text = "Refresh";
            this.btnRefrash.UseVisualStyleBackColor = true;
            this.btnRefrash.Click += new System.EventHandler(this.btnRefrash_Click);
            // 
            // gbMQTT
            // 
            this.gbMQTT.BackColor = System.Drawing.Color.White;
            this.gbMQTT.Controls.Add(this.txtBoxTopic);
            this.gbMQTT.Controls.Add(this.label2);
            this.gbMQTT.Controls.Add(this.txtBoxHost);
            this.gbMQTT.Controls.Add(this.label4);
            this.gbMQTT.Controls.Add(this.btnOpenHost);
            this.gbMQTT.Controls.Add(this.btnCloseHost);
            this.gbMQTT.Controls.Add(this.label6);
            this.gbMQTT.Controls.Add(this.label7);
            this.gbMQTT.Controls.Add(this.ledSendMQTT);
            this.gbMQTT.Controls.Add(this.lbStatusHost);
            this.gbMQTT.Enabled = false;
            this.gbMQTT.Location = new System.Drawing.Point(12, 118);
            this.gbMQTT.Name = "gbMQTT";
            this.gbMQTT.Size = new System.Drawing.Size(300, 132);
            this.gbMQTT.TabIndex = 13;
            this.gbMQTT.TabStop = false;
            this.gbMQTT.Text = "MQTT";
            // 
            // txtBoxTopic
            // 
            this.txtBoxTopic.Location = new System.Drawing.Point(45, 43);
            this.txtBoxTopic.Name = "txtBoxTopic";
            this.txtBoxTopic.Size = new System.Drawing.Size(245, 20);
            this.txtBoxTopic.TabIndex = 11;
            this.txtBoxTopic.Text = "bangfai_sensor";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Status:";
            // 
            // txtBoxHost
            // 
            this.txtBoxHost.Location = new System.Drawing.Point(45, 16);
            this.txtBoxHost.Name = "txtBoxHost";
            this.txtBoxHost.Size = new System.Drawing.Size(127, 20);
            this.txtBoxHost.TabIndex = 9;
            this.txtBoxHost.Text = "164.115.43.87";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Host:";
            // 
            // btnOpenHost
            // 
            this.btnOpenHost.Location = new System.Drawing.Point(178, 15);
            this.btnOpenHost.Name = "btnOpenHost";
            this.btnOpenHost.Size = new System.Drawing.Size(55, 23);
            this.btnOpenHost.TabIndex = 2;
            this.btnOpenHost.Text = "Open";
            this.btnOpenHost.UseVisualStyleBackColor = true;
            this.btnOpenHost.Click += new System.EventHandler(this.btnOpenHost_Click);
            // 
            // btnCloseHost
            // 
            this.btnCloseHost.Enabled = false;
            this.btnCloseHost.Location = new System.Drawing.Point(236, 15);
            this.btnCloseHost.Name = "btnCloseHost";
            this.btnCloseHost.Size = new System.Drawing.Size(55, 23);
            this.btnCloseHost.TabIndex = 3;
            this.btnCloseHost.Text = "Close";
            this.btnCloseHost.UseVisualStyleBackColor = true;
            this.btnCloseHost.EnabledChanged += new System.EventHandler(this.btnCloseHost_EnabledChanged);
            this.btnCloseHost.Click += new System.EventHandler(this.btnCloseHost_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 97);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 18);
            this.label6.TabIndex = 8;
            this.label6.Text = "Send MQTT :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Status:";
            // 
            // ledSendMQTT
            // 
            this.ledSendMQTT.BackColor = System.Drawing.Color.Gray;
            this.ledSendMQTT.Location = new System.Drawing.Point(150, 95);
            this.ledSendMQTT.Name = "ledSendMQTT";
            this.ledSendMQTT.Size = new System.Drawing.Size(113, 25);
            this.ledSendMQTT.TabIndex = 7;
            // 
            // lbStatusHost
            // 
            this.lbStatusHost.AutoSize = true;
            this.lbStatusHost.Location = new System.Drawing.Point(55, 76);
            this.lbStatusHost.Name = "lbStatusHost";
            this.lbStatusHost.Size = new System.Drawing.Size(10, 13);
            this.lbStatusHost.TabIndex = 5;
            this.lbStatusHost.Text = "-";
            // 
            // tmMQTT
            // 
            this.tmMQTT.Interval = 200;
            this.tmMQTT.Tick += new System.EventHandler(this.tmMQTT_Tick);
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(112, 64);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(75, 23);
            this.btnTest.TabIndex = 14;
            this.btnTest.Text = "Public";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // gbTest
            // 
            this.gbTest.BackColor = System.Drawing.Color.White;
            this.gbTest.Controls.Add(this.label8);
            this.gbTest.Controls.Add(this.txtBoxPayload);
            this.gbTest.Controls.Add(this.btnTest);
            this.gbTest.Enabled = false;
            this.gbTest.Location = new System.Drawing.Point(12, 285);
            this.gbTest.Name = "gbTest";
            this.gbTest.Size = new System.Drawing.Size(300, 97);
            this.gbTest.TabIndex = 15;
            this.gbTest.TabStop = false;
            this.gbTest.Text = "TEST";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Payload:";
            // 
            // txtBoxPayload
            // 
            this.txtBoxPayload.Location = new System.Drawing.Point(14, 38);
            this.txtBoxPayload.Name = "txtBoxPayload";
            this.txtBoxPayload.Size = new System.Drawing.Size(277, 20);
            this.txtBoxPayload.TabIndex = 15;
            this.txtBoxPayload.Text = "#S1,13.189267,100.975472,10,10,51,151515,12,12,12";
            // 
            // cbTest
            // 
            this.cbTest.AutoSize = true;
            this.cbTest.Enabled = false;
            this.cbTest.Location = new System.Drawing.Point(13, 257);
            this.cbTest.Name = "cbTest";
            this.cbTest.Size = new System.Drawing.Size(69, 17);
            this.cbTest.TabIndex = 16;
            this.cbTest.Text = "Use Test";
            this.cbTest.UseVisualStyleBackColor = true;
            this.cbTest.CheckStateChanged += new System.EventHandler(this.cbTest_CheckStateChanged);
            // 
            // SendMQTT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(324, 385);
            this.Controls.Add(this.cbTest);
            this.Controls.Add(this.gbTest);
            this.Controls.Add(this.gbMQTT);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SendMQTT";
            this.Text = "Bangfai  Application";
            this.Load += new System.EventHandler(this.SendMQTT_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbMQTT.ResumeLayout(false);
            this.gbMQTT.PerformLayout();
            this.gbTest.ResumeLayout(false);
            this.gbTest.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboPort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnOpenCP;
        private System.Windows.Forms.Button btnCloseCP;
        private System.IO.Ports.SerialPort serialPort;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbStatusCP;
        private System.Windows.Forms.Panel ledRecived;
        private System.Windows.Forms.Timer tmRecive;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbMQTT;
        private System.Windows.Forms.TextBox txtBoxHost;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnOpenHost;
        private System.Windows.Forms.Button btnCloseHost;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel ledSendMQTT;
        private System.Windows.Forms.Label lbStatusHost;
        private System.Windows.Forms.Timer tmMQTT;
        private System.Windows.Forms.TextBox txtBoxTopic;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.GroupBox gbTest;
        private System.Windows.Forms.TextBox txtBoxPayload;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox cbTest;
        private System.Windows.Forms.Button btnRefrash;
    }
}

